import {ref} from "vue";

export default function useTransactions() {
    const balance = ref([])
    const transactions = ref([])

    const load = async (sort = 'asc') => {
        await axios
            .get(`/api/v1/users/transaction-histories?sort_by=${sort}`)
            .then((response) => {
                balance.value = response.data.balance
                transactions.value = response.data.transactions
            })
            .catch((e) => {
                console.log('Loading Error', e)
            })
    }

    return {
        balance,
        transactions,
        load
    }
}