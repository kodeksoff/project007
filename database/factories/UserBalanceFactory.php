<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\UserBalance;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserBalanceFactory extends Factory
{
    protected $model = UserBalance::class;

    public function definition(): array
    {
        return [

        ];
    }
}
