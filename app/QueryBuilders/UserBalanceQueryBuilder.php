<?php

declare(strict_types=1);

namespace App\QueryBuilders;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class UserBalanceQueryBuilder extends Builder
{
    public function latestBalance(User $user): self
    {
        return $this
            ->where('user_id', $user->id)
            ->latest();
    }
}