<?php

declare(strict_types=1);

namespace App\QueryBuilders;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class TransactionQueryBuilder extends Builder
{
    public function latestTransaction(User $user): self
    {
        return $this
            ->where('user_id', $user->id)
            ->latest();
    }
}