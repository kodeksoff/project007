<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Actions\Users\GetUserTransactions;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TransactionHistoryController extends Controller
{
    public function __invoke(Request $request, GetUserTransactions $getUserTransactions)
    {
        return new JsonResponse(
            $getUserTransactions($request->get('sort_by', 'asc'))
        );
    }
}
