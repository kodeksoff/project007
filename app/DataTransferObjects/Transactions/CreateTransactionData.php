<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Transactions;

use App\Enums\TransactionType;
use App\Models\User;
use App\Utils\DataCasts\MoneyCast;
use Brick\Money\Money;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Casts\EnumCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class CreateTransactionData extends Data
{
    public function __construct(
        public readonly User $user,
        #[WithCast(EnumCast::class)]
        public readonly TransactionType $type,
        #[WithCast(MoneyCast::class)]
        public readonly Money $amount,
    ) {
    }
}