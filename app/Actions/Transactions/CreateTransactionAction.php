<?php

declare(strict_types=1);

namespace App\Actions\Transactions;

use App\Actions\UserBalances\CheckCurrentBalanceAction;
use App\Actions\UserBalances\StoreUserBalanceAction;
use App\DataTransferObjects\Transactions\CreateTransactionData;
use App\Enums\TransactionType;
use Illuminate\Database\DatabaseManager;
use Throwable;

class CreateTransactionAction
{
    public function __construct(
        private DatabaseManager $databaseManager,
        private CheckCurrentBalanceAction $checkCurrentBalanceAction,
        private StoreTransactionAction $storeTransactionAction,
        private StoreUserBalanceAction $storeUserBalanceAction,
    ) {
    }

    /**
     * @throws Throwable
     */
    public function __invoke(CreateTransactionData $createTransactionData): void
    {
        if ($createTransactionData->type === TransactionType::CONSUMPTION) {
            ($this->checkCurrentBalanceAction)(
                $createTransactionData->user,
                $createTransactionData->amount,
            );
        }

        $this
            ->databaseManager
            ->transaction(function () use ($createTransactionData) {
                ($this->storeUserBalanceAction)($createTransactionData);
                ($this->storeTransactionAction)($createTransactionData);
            });
    }
}