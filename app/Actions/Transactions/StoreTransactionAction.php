<?php

declare(strict_types=1);

namespace App\Actions\Transactions;

use App\DataTransferObjects\Transactions\CreateTransactionData;
use App\Enums\TransactionType;
use App\Factories\MoneyFactory;
use App\Models\Transaction;
use Brick\Math\Exception\MathException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\MoneyMismatchException;
use Brick\Money\Exception\UnknownCurrencyException;

class StoreTransactionAction
{
    /**
     * @param  MoneyFactory  $moneyFactory
     */
    public function __construct(private MoneyFactory $moneyFactory)
    {
    }

    /**
     * @param  CreateTransactionData  $createTransactionData
     *
     * @return Transaction
     * @throws MathException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     * @throws MoneyMismatchException
     * @throws UnknownCurrencyException
     */
    public function __invoke(CreateTransactionData $createTransactionData): Transaction
    {
        $latestAmount = Transaction::query()
            ->latestTransaction($createTransactionData->user)
            ->first()
            ?->current_amount
            ?? $this
                ->moneyFactory
                ->ofMinor(0);

        $newAmount = $createTransactionData->amount;

        if ($createTransactionData->type === TransactionType::COMING) {
            $newAmount = $createTransactionData
                ->amount
                ->plus($latestAmount);
        }

        if ($createTransactionData->type === TransactionType::CONSUMPTION) {
            $newAmount = $latestAmount->minus($createTransactionData->amount);
        }

        return Transaction::query()
            ->create([
                'user_id' => $createTransactionData->user->id,
                'type' => $createTransactionData->type,
                'current_amount' => $newAmount,
                'old_amount' => $latestAmount,
            ]);
    }
}