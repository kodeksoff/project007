<?php

declare(strict_types=1);

namespace App\Actions\UserBalances;

use App\DataTransferObjects\Transactions\CreateTransactionData;
use App\Enums\TransactionType;
use App\Factories\MoneyFactory;
use App\Models\UserBalance;
use Brick\Math\Exception\MathException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\MoneyMismatchException;
use Brick\Money\Exception\UnknownCurrencyException;

class StoreUserBalanceAction
{
    /**
     * @param  MoneyFactory  $moneyFactory
     */
    public function __construct(private MoneyFactory $moneyFactory)
    {
    }

    /**
     * @param  CreateTransactionData  $createTransactionData
     *
     * @return UserBalance
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     * @throws UnknownCurrencyException
     * @throws MathException
     * @throws MoneyMismatchException
     */
    public function __invoke(CreateTransactionData $createTransactionData): UserBalance
    {
        $oldBalance = UserBalance::query()
            ->latestBalance($createTransactionData->user)
            ->first()
            ?->current_balance
            ?? $this
                ->moneyFactory
                ->ofMinor(0);

        $newBalance = $createTransactionData->amount;

        if ($createTransactionData->type === TransactionType::COMING) {
            $newBalance = $createTransactionData
                ->amount
                ->plus($oldBalance);
        }

        if ($createTransactionData->type === TransactionType::CONSUMPTION) {
            $newBalance = $oldBalance->minus($createTransactionData->amount);
        }

        return UserBalance::query()
            ->create([
                'user_id' => $createTransactionData->user->id,
                'current_balance' => $newBalance,
                'old_balance' => $oldBalance,
            ]);
    }
}