<?php

declare(strict_types=1);

namespace App\Actions\UserBalances;

use App\Factories\MoneyFactory;
use App\Models\User;
use App\Models\UserBalance;
use Brick\Money\Money;
use Exception;
use Throwable;

class CheckCurrentBalanceAction
{
    /**
     * @throws Throwable
     */
    public function __invoke(User $user, Money $money): void
    {
        $userBalance = UserBalance::query()
            ->latestBalance($user)
            ->first();

        throw_if(
            !$userBalance
            || $userBalance
                ->current_balance
                ->minus($money)
                ->getMinorAmount()
                ->toInt() < 0,
            new Exception('В результате транзакции баланс окажется меньше нуля'),
        );
    }
}