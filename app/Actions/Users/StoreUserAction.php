<?php

declare(strict_types=1);

namespace App\Actions\Users;

use App\DataTransferObjects\Users\StoreUserData;
use App\Models\User;
use Illuminate\Contracts\Hashing\Hasher;

class StoreUserAction
{
    public function __construct(private Hasher $hasher)
    {
    }

    public function __invoke(StoreUserData $storeUserData): User
    {
        return User::query()
            ->create([
                'name' => $storeUserData->name,
                'email' => $storeUserData->email,
                'password' => $this
                    ->hasher
                    ->make($storeUserData->password),
            ]);
    }
}