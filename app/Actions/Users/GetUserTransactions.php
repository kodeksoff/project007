<?php

declare(strict_types=1);

namespace App\Actions\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class GetUserTransactions
{
    public function __invoke(string $direction = 'asc'): User
    {
        return User::query()
            ->with([
                'balance',
                'transactions' =>
                    fn (Builder|HasMany $builder): Builder|HasMany => $builder
                        ->limit(5)
                        ->orderBy('id', $direction),
            ])
            ->first();
    }
}