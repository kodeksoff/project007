<?php

declare(strict_types=1);

namespace App\Enums;

enum TransactionType: string
{
    case COMING = 'coming';
    case CONSUMPTION = 'consumption';
}