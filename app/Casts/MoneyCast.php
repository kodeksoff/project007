<?php

declare(strict_types=1);

namespace App\Casts;

use App\Factories\MoneyFactory;
use Brick\Math\Exception\MathException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money as BrickMoney;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;

class MoneyCast implements CastsAttributes
{

    /**
     * @throws UnknownCurrencyException
     * @throws RoundingNecessaryException
     * @throws NumberFormatException
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): ?BrickMoney
    {
        $fields = json_decode($value);

        return resolve(MoneyFactory::class)->ofMinor(
            $fields->amount,
            $fields->currency,
        );
    }

    /**
     * @throws MathException
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): string|false
    {
        /** @var BrickMoney $value */
        return json_encode([
            'amount' => $value
                ->getMinorAmount()
                ->toInt(),
            'currency' => $value
                ->getCurrency()
                ->getCurrencyCode(),
        ]);
    }
}