<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Actions\Transactions\CreateTransactionAction;
use App\DataTransferObjects\Transactions\CreateTransactionData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class TransactionJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private CreateTransactionData $createTransactionData)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @throws Throwable
     */
    public function handle(CreateTransactionAction $createTransactionAction): void
    {
        $createTransactionAction($this->createTransactionData);
    }

    /**
     * @return string
     */
    public function uniqueId(): string
    {
        return sprintf(
            'transaction_user_id_%s',
            $this
                ->createTransactionData
                ->user
                ->id,
        );
    }
}
