<?php

declare(strict_types=1);

namespace App\Models;

use App\Casts\MoneyCast;
use App\Enums\TransactionType;
use App\QueryBuilders\UserBalanceQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserBalance extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'current_balance',
        'old_balance',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'current_balance' => MoneyCast::class,
        'old_balance' => MoneyCast::class,
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param $query
     *
     * @return UserBalanceQueryBuilder
     */
    public function newEloquentBuilder($query): UserBalanceQueryBuilder
    {
        return new UserBalanceQueryBuilder($query);
    }
}
