<?php

declare(strict_types=1);

namespace App\Utils\DataCasts;

use App\Factories\MoneyFactory;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money;
use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\DataProperty;

class MoneyCast implements Cast
{
    /**
     * @throws UnknownCurrencyException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     */
    public function cast(DataProperty $property, mixed $value, array $context): Money
    {
        return resolve(MoneyFactory::class)->ofMinor($value);
    }
}
