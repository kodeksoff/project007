<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Transactions\CreateTransactionAction;
use App\DataTransferObjects\Transactions\CreateTransactionData;
use App\Enums\TransactionType;
use App\Jobs\TransactionJob;
use App\Models\User;
use Illuminate\Bus\Dispatcher;
use Illuminate\Console\Command;

class CreateTransactionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create transaction';

    public function __construct(private Dispatcher $dispatcher)
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(CreateTransactionAction $createTransactionAction): void
    {
        $email = $this->ask('Введите Email пользователя');

        $transactionType = $this->choice(
            'Введите тип транзакции',
            [
                TransactionType::COMING->value,
                TransactionType::CONSUMPTION->value,
            ],
        );

        $amount = $this->ask('Укажите сумму транзакции');

        if (!is_numeric($amount)) {
            $this->error('Сумма содержит недопустимые символы');
        }

        $user = User::query()
            ->where('email', $email)
            ->firstOrFail();

        $this
            ->dispatcher
            ->dispatch(
                new TransactionJob(
                    CreateTransactionData::from([
                        'user' => $user,
                        'type' => $transactionType,
                        'amount' => $amount,
                    ]),
                ),
            );
    }
}
