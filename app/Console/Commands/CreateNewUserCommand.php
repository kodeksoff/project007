<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\Users\StoreUserAction;
use App\DataTransferObjects\Users\StoreUserData;
use Illuminate\Console\Command;

class CreateNewUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-user {name?} {email?} {password?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Execute the console command.
     */
    public function handle(StoreUserAction $storeUserAction): void
    {
        if (!$name = $this->argument('name')) {
            $name = $this->ask('Укажите имя пользователя');
        }

        if (!$email = $this->argument('email')) {
            $email = $this->ask('Укажите Email пользователя');
        }

        if (!$password = $this->argument('password')) {
            $password = $this->secret('Укажите пароль');
        }

        if (!$name || !$email || !$password) {
            $this->error('Пожалуйста, заполните все поля');
            return;
        }

        $storeUserAction(
            new StoreUserData(
                name: $name,
                email: $email,
                password: $password,
            ),
        );
    }
}
